﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace OnlinePhoneBook
{
    public partial class UserInterface : Form
    {
        private string _username;
        public void GetUserName(string user)
        {
            this._username = user;
        }


        public UserInterface()
        {
            InitializeComponent();
            Display();
        }


        private void Display()
        {
            dataGridView1.Rows.Clear();
            UserService user = new UserService();
            var connect = user.Connect();
            MySqlDataAdapter msa = new MySqlDataAdapter("SELECT ID, FirstName, LastName, Phone, Email, Category FROM Phones WHERE Username ='" + _username + "'", connect);
            DataTable dt = new DataTable();
            msa.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                int n = dataGridView1.Rows.Add();
                dataGridView1.Rows[n].Cells[0].Value = row[0].ToString();
                dataGridView1.Rows[n].Cells[1].Value = row[1].ToString();
                dataGridView1.Rows[n].Cells[2].Value = row[2].ToString();
                dataGridView1.Rows[n].Cells[3].Value = row[3].ToString();
                dataGridView1.Rows[n].Cells[4].Value = row[4].ToString();
                dataGridView1.Rows[n].Cells[5].Value = row[5].ToString();
            }
        }

        private void ShowMyContact_Click(object sender, EventArgs e)
        {
            Display();
        }

        private void AddNewContact_Click(object sender, EventArgs e)
        {
            UserService user = new UserService();
            var connect = user.Connect();
            connect.Open();
            MySqlCommand msc = new MySqlCommand("INSERT INTO Phones (FirstName, LastName, Phone, Email, Category, Username) VALUES('"+firstNameTextBox.Text+"','"+lasteNameTextBox.Text+"','"+phoneTextBox.Text+"','"+emailTextBox.Text+"','"+categoryTextBox.Text+"','"+_username+"')", connect);
            msc.ExecuteNonQuery();
            MessageBox.Show("Contact Successfully Added");
            firstNameTextBox.Clear();
            lasteNameTextBox.Clear();
            phoneTextBox.Clear();
            emailTextBox.Clear();
            categoryTextBox.Clear();
            Display();

        }
    }
}
