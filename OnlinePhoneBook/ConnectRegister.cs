﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace OnlinePhoneBook
{
    public partial class ConnectRegister : Form
    {
        public ConnectRegister()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            LoginInfo loginInfo = new LoginInfo();
            loginInfo.Show();
            this.Hide();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RegisterInfo register = new RegisterInfo();
            register.Show();
            this.Hide();
        }
    }
}
