﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace OnlinePhoneBook
{
    public partial class WelcomePage : Form
    {
       
        public WelcomePage()
        {
            InitializeComponent();
        }

        private void connect_Click(object sender, EventArgs e)
        {
            
            UserService user = new UserService();
            var connect = user.Connect();
            connect.Open();
            if (connect.State == ConnectionState.Open)
            {
                    MessageBox.Show("You are Now connected to your DataBase");
                    ShowHide();
                  
            }

        }

        private void ShowHide()
        {
            ConnectRegister aNForm = new ConnectRegister();
            aNForm.Show();
            this.Hide();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        
    }
}
