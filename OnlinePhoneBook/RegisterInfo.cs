﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace OnlinePhoneBook
{
    public partial class RegisterInfo : Form
    {
        public RegisterInfo()
        {
            InitializeComponent();
           
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            LoginInfo loginInfo = new LoginInfo();
            UserService con = new UserService();
            var connexion = con.Connect();
            connexion.Open();
            MySqlCommand msc = new MySqlCommand("SELECT Username FROM UsernamesTable", connexion);
            msc.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(msc);
            da.Fill(dt);

            bool isNotRepeated=true;
            bool isAvailable = true;

            foreach (DataRow row in dt.Rows)
            {
                string name = row[0].ToString();
                if (name.Equals(usernameTextBox.Text))
                {
                    MessageBox.Show("Please choose another Username, this one is already taken");
                    isAvailable = false;
                    
                }
                else if (passwordTextBox.Text != rePasswordTextBox.Text)
                {
                    MessageBox.Show("The passwords doesn't match, Try again");
                    isNotRepeated = false;
                }
            }

            if (isAvailable == true || isNotRepeated == true)
            {
                msc = new MySqlCommand("INSERT INTO `UsernamesTable` (Username, Password) VALUES ('" + usernameTextBox.Text + "','" + passwordTextBox.Text + "')", connexion);
                msc.ExecuteNonQuery();
                MessageBox.Show("You've succesfully registred as : " + usernameTextBox.Text+" Please use your information to connect to your PhoneBook.");
                this.Close();
                loginInfo.Show();


            }


        }

        
    }
}
