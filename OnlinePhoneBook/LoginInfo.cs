﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace OnlinePhoneBook
{
    public partial class LoginInfo : Form
    {

        public LoginInfo()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            UserInterface ui = new UserInterface();
            UserService user = new UserService();
            var connect = user.Connect();
            connect.Open();
            MySqlCommand msc = new MySqlCommand("Select Username,Password From UsernamesTable", connect);
            msc.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(msc);
            da.Fill(dt);

            
            foreach (DataRow raw in dt.Rows)
            {

                if (raw[0].ToString() == Username.Text)
                {
                    if (raw[1].ToString() == Password.Text)
                    {
                        MessageBox.Show("You've successufly Connected");
                        ui.GetUserName(Username.Text);
                        this.Hide();
                        ui.Show();
                    }
                    else
                    {
                        MessageBox.Show("Your password is incorrect");

                    }
                }
            }

        }
    }
}
